# Introducción a la Programacion con Python #

* Software (Instalaciones necesarias para el curso)
* Ejercicios (Ejercicios y tareas)
* Clases (Presentaciones de la clase)

### Instrucciones del material:

Para utilizar el material del curso recomiendo:

* Descargar todo el material:
Al lado izquierdo en la página del repositorio aparecer un ícono que permite descargar todo el material
("Downloads") del repositorio a su computador.

* Clonar el Repositorio:
Al lado izquierdo en la página del repositorio aparecer un ícono que permite Clonar ("Clone") 
el repositorio a su computador.

Al realizar cualquiera de estas acciones puede abrir el contenido fácilmente,i.e., 

- .html (Con un explorador como Firefox o Chrome)

- .ipynb (con una terminal)

Durante el curso se explicará cómo mantener el repositorio actualizado. Las referencias están en cada documento.

### My Binder ###

Para estudiar este material de manera remota siguiendo este link:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Falejandroc137%2Fcursopython.git/420f4d78ec09d396eec44c16385fe80d34b60c67)

#### Contacto: ####

Alejandro **Cárdenas-Avendaño**

<em>alejandro.cardenasa@konradlorenz.edu.co</em>